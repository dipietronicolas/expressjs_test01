const express = require('express');
const route = require('./routes/index');
const app = express();


// Settings
app.set('name', 'MyAppExpressJs');
app.set('view engine', 'ejs');

// Middleware
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use('/home/', route);
//app.use(express.static('public'));

app.use(express.static('views'));
//app.use(express.static('views/css'));



app.listen(3000, function () {
  console.log('Example app listening on port 3000!');
  console.log(app.get('name'));
});