const express = require('express');
const mysql = require('mysql');
const router = express.Router();



// Database settings
const connection = mysql.createConnection({
    host: 'localhost',
    database: 'express',
    user: '',
    password: ''
});

// Metodos
router.get('/', (req, res) => {
    res.render('index.ejs');
});

// Pagina de registro
router.get('/register/', (req, res) => {
    res.render('register.ejs');
});

// Pagina de usuario
router.get('/user/', (req, res) => {
    res.render('user.ejs');
});

//Recibo los datos del formulario con metodo post
router.post('/register/', (req, res, next) => {
    console.log(req.body);
    res.json(req.body);
});

router.get('/consulta/', (req, res) => {
    connection.connect();

    connection.query('SELECT * FROM users WHERE id = 1', (err, results) => {
        if (err) throw err;
        console.log('The solution is: ', results);
        res.send(results);
    });
    connection.end();
    //res.send('Esta todo bien.');
    
});

module.exports = router;